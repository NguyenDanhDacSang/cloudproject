package com.sangndd.controller.matchhistory;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sangndd.model.dto.matchhistory.MatchHistoryDTO;
import com.sangndd.model.dto.matchhistory.statistics.MatchHistoryStatisticsDTO;
import com.sangndd.model.service.matchhistory.MatchHistoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/cols/api/match-history")
@CrossOrigin
@Api(value = "Match History API")
public class MatchHistoryController {

	@Autowired
	private MatchHistoryService matchHistoryService;

	@GetMapping("statitics")
	@ApiOperation(response = ResponseEntity.class, value = "Returns the match statitics (including total matches, the number of matches where player won, lost or drew against bot) within an intervals of days and skill level of bot.")
	public ResponseEntity<MatchHistoryStatisticsDTO> countStatiticsWithinIntervals(
			@ApiParam(value = "(Optional) A number in range of [1-20] defines the lower boundary of bot skill level intervals. Will query from lowest skill level if not set.") @RequestParam(name = "skill-level-lower-bound", required = false) Integer skillLevelFrom,
			@ApiParam(value = "(Optional) A number in range of [1-20] defines the upper boundary of bot skill level intervals. Will query to highest skill level if not set.") @RequestParam(name = "skill-level-upper-bound", required = false) Integer skillLevelTo,
			@ApiParam(value = "(Optional) A number in range of [1-20] defines the lower boundary of bot move search engine depth intevals. Will query from lowest depth if not set.") @RequestParam(name = "bot-depth-lower-bound", required = false) Integer botDepthFrom,
			@ApiParam(value = "(Optional) A number in range of [1-20] defines the upper boundary of bot move search engine depth intevals. Will query to highest depth if not set.") @RequestParam(name = "bot-depth-upper-bound", required = false) Integer botDepthTo,
			@ApiParam(value = "(Required) A String in format 2019-09-13T15:30:09.0000 defines the lower boundary of query day intervals") @RequestParam(name = "query-from-day") LocalDateTime dayFrom,
			@ApiParam(value = "(Required) A String in format 2019-09-13T15:30:09.0000 defines the upper boundary of query day intervals") @RequestParam(name = "query-to-day") LocalDateTime dayTo) {
		MatchHistoryStatisticsDTO res = matchHistoryService.countStatitics(skillLevelFrom, skillLevelTo, botDepthFrom,
				botDepthTo, dayFrom, dayTo);

		return ResponseEntity.ok().body(res);
	}

	@PostMapping("create")
	@ApiOperation(response = ResponseEntity.class, value = "Persists the match history to database.")
	public ResponseEntity<MatchHistoryDTO> create(
			@ApiParam(value = "A JSON to describes match history. \nNote:\n + Due to ID automatic generation, ID should not be included.\n + Created time if included must be in format 2019-09-13T15:30:09.0000.") @RequestBody MatchHistoryDTO dto)
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException,
			ParseException {
		MatchHistoryDTO res = matchHistoryService.create(dto);

		return ResponseEntity.ok().body(res);
	}

	@GetMapping("get")
	@ApiOperation(response = ResponseEntity.class, value = "Find all match history with paging and sorting.")
	public ResponseEntity<List<MatchHistoryDTO>> findAll()
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		List<MatchHistoryDTO> res = matchHistoryService.findAll();

		return ResponseEntity.ok().body(res);
	}

	@SuppressWarnings("unchecked")
	@GetMapping("find")
	@ApiOperation(response = ResponseEntity.class, value = "Find a specific match history using ID")
	public ResponseEntity<MatchHistoryDTO> findById(
			@ApiParam(value = "A string defines the match history ID to find.") @RequestParam String id)
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		MatchHistoryDTO res = matchHistoryService.findById(id);

		if (null != res) {
			return ResponseEntity.ok().body(res);
		} else {
			return (ResponseEntity<MatchHistoryDTO>) ResponseEntity.notFound();
		}
	}

}
