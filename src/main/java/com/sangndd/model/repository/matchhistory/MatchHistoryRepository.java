package com.sangndd.model.repository.matchhistory;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import com.sangndd.model.entity.matchhistory.MatchHistoryEntity;

@Repository
public interface MatchHistoryRepository extends PagingAndSortingRepository<MatchHistoryEntity, String> {

	/* Find Between */
	List<MatchHistoryEntity> findByBotDepthBetween(int botDepthFrom, int botDepthTo);

	List<MatchHistoryEntity> findByCreatedDateBetween(LocalDateTime startDate, LocalDateTime endDate);

	List<MatchHistoryEntity> findBySkillLevelBetween(int skillLevelFrom, int skillLevelTo);
	/* ---------- Line Break ---------- */

	/* Find Greater Than or Equal */
	List<MatchHistoryEntity> findByBotDepthGreaterThanEqual(int botDepthFrom);

	List<MatchHistoryEntity> findBySkillLevelGreaterThanEqual(int skillLevelFrom);
	/* ---------- Line Break ---------- */

	/* Find Less Than orEqual */
	List<MatchHistoryEntity> findByBotDepthLessThanEqual(int botDepthTo);

	List<MatchHistoryEntity> findBySkillLevelLessThanEqual(int skillLevelTo);
	/* ---------- Line Break ---------- */

}
