package com.sangndd.model.service.matchhistory;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Service;

import com.sangndd.model.dto.matchhistory.MatchHistoryDTO;
import com.sangndd.model.dto.matchhistory.statistics.MatchHistoryStatisticsDTO;

@Service
public interface MatchHistoryService {

	MatchHistoryStatisticsDTO countStatitics(Integer skillLevelFrom, Integer skillLevelTo, Integer botDepthFrom,
			Integer botDepthTo, LocalDateTime dayFrom, LocalDateTime dayTo);

	MatchHistoryDTO create(MatchHistoryDTO dto) throws ParseException, IllegalArgumentException, IllegalAccessException,
			NoSuchFieldException, SecurityException;

	List<MatchHistoryDTO> findAll()
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException;

	MatchHistoryDTO findById(String id)
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException;

}
