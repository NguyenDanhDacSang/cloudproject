package com.sangndd.model.service.matchhistory.impl;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sangndd.model.dto.matchhistory.MatchHistoryDTO;
import com.sangndd.model.dto.matchhistory.statistics.MatchHistoryStatisticsDTO;
import com.sangndd.model.entity.matchhistory.MatchHistoryEntity;
import com.sangndd.model.repository.matchhistory.MatchHistoryRepository;
import com.sangndd.model.service.matchhistory.MatchHistoryService;
import com.sangndd.model.utils.Constant;
import com.sangndd.model.utils.ModelConverter;

@Service
public class MatchHistoryServiceImpl implements MatchHistoryService {

	@Autowired
	private MatchHistoryRepository matchHistoryRepository;

	@Override
	public MatchHistoryStatisticsDTO countStatitics(Integer skillLevelFrom, Integer skillLevelTo, Integer botDepthFrom,
			Integer botDepthTo, LocalDateTime dayFrom, LocalDateTime dayTo) {
		MatchHistoryStatisticsDTO res = new MatchHistoryStatisticsDTO();

		// Get all match histories within interval days (total matches within days)
		List<MatchHistoryEntity> matchHistoriesWithinDays = matchHistoryRepository.findByCreatedDateBetween(dayFrom,
				dayTo);
		res.setTotalMatch(matchHistoriesWithinDays.size());
		/* ---------- Line Break ---------- */

		// Get all match histories below skill level upper boundary
		List<MatchHistoryEntity> matchHistoriesBelowIntervals;
		if (null != skillLevelTo) { // Has upper boundary
			matchHistoriesBelowIntervals = matchHistoryRepository.findBySkillLevelLessThanEqual(skillLevelTo);
		} else { // No upper boundary
			matchHistoriesBelowIntervals = new LinkedList<>(matchHistoriesWithinDays);
		}
		// Get all match histories above skill level lower boundary
		List<MatchHistoryEntity> matchHistoriesAboveIntevals;
		if (null != skillLevelFrom) { // Has lower boundary
			matchHistoriesAboveIntevals = matchHistoryRepository.findBySkillLevelGreaterThanEqual(skillLevelFrom);
		} else { // No lower boundary
			matchHistoriesAboveIntevals = new LinkedList<>(matchHistoriesWithinDays);
			;
		}
		// Get all match histories within skill level intervals
		List<MatchHistoryEntity> matchHistoriesWithinSkillLevels = (List<MatchHistoryEntity>) CollectionUtils
				.intersection(matchHistoriesBelowIntervals, matchHistoriesAboveIntevals);
		/* ---------- Line Break ---------- */

		// Get all match histories below skill level upper boundary
		matchHistoriesBelowIntervals = null;
		if (null != skillLevelTo) { // Has upper boundary
			matchHistoriesBelowIntervals = matchHistoryRepository.findByBotDepthLessThanEqual(skillLevelTo);
		} else { // No upper boundary
			matchHistoriesBelowIntervals = new LinkedList<>(matchHistoriesWithinDays);
			;
		}
		// Get all match histories above skill level lower boundary
		matchHistoriesAboveIntevals = null;
		if (null != skillLevelFrom) { // Has lower boundary
			matchHistoriesAboveIntevals = matchHistoryRepository.findByBotDepthGreaterThanEqual(skillLevelFrom);
		} else { // No lower boundary
			matchHistoriesAboveIntevals = new LinkedList<>(matchHistoriesWithinDays);
			;
		}
		// Get all match histories within bot depths intervals
		List<MatchHistoryEntity> matchHistoriesWithinBotDepths = (List<MatchHistoryEntity>) CollectionUtils
				.intersection(matchHistoriesBelowIntervals, matchHistoriesAboveIntevals);
		/* ---------- Line Break ---------- */

		// Intersect match histories within interval days and skill levels
		List<MatchHistoryEntity> matchHistoriesWithinIntevals = (List<MatchHistoryEntity>) CollectionUtils
				.intersection(matchHistoriesWithinDays, matchHistoriesWithinSkillLevels);
		// Continuous intersect with bot depths
		matchHistoriesWithinIntevals = (List<MatchHistoryEntity>) CollectionUtils
				.intersection(matchHistoriesWithinIntevals, matchHistoriesWithinBotDepths);
		/* ---------- Line Break ---------- */

		// Get match history with result (won, lost, drew)
		List<MatchHistoryEntity> matchHistoryDrew = new LinkedList<>(matchHistoriesWithinIntevals);
		List<MatchHistoryEntity> matchHistoryLost = new LinkedList<>(matchHistoriesWithinIntevals);
		List<MatchHistoryEntity> matchHistoryWon = new LinkedList<>(matchHistoriesWithinIntevals);
		for (MatchHistoryEntity entity : matchHistoriesWithinIntevals) {
			if (entity.getResult().equalsIgnoreCase(Constant.MatchResult.DRAW)) { // Draw match
				matchHistoryLost.remove(entity);
				matchHistoryWon.remove(entity);
			} else {
				// Check player win (player side same color with result)
				boolean isPlayerWon = (((entity.isPlayerSideWhite()) // Player side White
						&& (entity.getResult().equalsIgnoreCase(Constant.MatchResult.WHITE_WON)))
						|| ((!entity.isPlayerSideWhite()) // Player side Black
								&& (entity.getResult().equalsIgnoreCase(Constant.MatchResult.BLACK_WON))));

				if (isPlayerWon) { // Player is won, remove from list lost and drew
					matchHistoryLost.remove(entity);
					matchHistoryDrew.remove(entity);
				} else { // Player is lost, remove from list won and drew
					matchHistoryWon.remove(entity);
					matchHistoryDrew.remove(entity);
				}
			}
		}

		// Set count for each match results
		res.setDrewMatch(matchHistoryDrew.size());
		res.setLostMatch(matchHistoryLost.size());
		res.setWonMatch(matchHistoryWon.size());
		/* ---------- Line Break ---------- */

		return res;
	}

	@Override
	public MatchHistoryDTO create(MatchHistoryDTO dto) throws ParseException, IllegalArgumentException,
			IllegalAccessException, NoSuchFieldException, SecurityException {
		// Set the current date time if it has not been set yet
		if (null == dto.getCreatedDate()) {
			LocalDateTime localDateTime = LocalDateTime.now();

			dto.setCreatedDate(localDateTime);
		}

		MatchHistoryEntity entity = (MatchHistoryEntity) ModelConverter.toEntity(dto);
		entity = matchHistoryRepository.save(entity);

		return (MatchHistoryDTO) ModelConverter.toDTO(entity);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MatchHistoryDTO> findAll()
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		List<MatchHistoryEntity> res = new LinkedList<>();
		matchHistoryRepository.findAll().forEach(res::add);
		
		return (List<MatchHistoryDTO>) ModelConverter.toDTOs(res);
	}

	@Override
	public MatchHistoryDTO findById(String id)
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		MatchHistoryEntity entity = matchHistoryRepository.findById(id).get();

		return (MatchHistoryDTO) ModelConverter.toDTO(entity);
	}

}
