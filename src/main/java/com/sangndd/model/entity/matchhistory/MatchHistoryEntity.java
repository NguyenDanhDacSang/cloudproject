package com.sangndd.model.entity.matchhistory;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.sangndd.model.entity.ModelEntity;

@Entity
@Table(name = "match_history", schema = "cols")
public class MatchHistoryEntity extends ModelEntity {

	private static final long serialVersionUID = -37704483168583651L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	@Column(name = "bot_depth")
	@NotNull
	private int botDepth;

	@Column(name = "skill_level")
	@NotNull
	private int skillLevel;

	@Column(name = "portable_game_notation")
	@Type(type = "org.hibernate.type.TextType")
	@NotNull
	private String portableGameNotation;

	@Column(name = "player_color")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	@NotNull
	private boolean playerSideWhite;

	@Column(name = "duration")
	@NotNull
	private int duration;

	@Column(name = "result")	
	@NotNull
	private String result;

	@Column(name = "created_date", columnDefinition = "TIMESTAMP")	
	@NotNull
	private LocalDateTime createdDate;

	public MatchHistoryEntity() {
	}

	public int getBotDepth() {
		return botDepth;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public int getDuration() {
		return duration;
	}

	public String getId() {
		return id;
	}

	public String getPortableGameNotation() {
		return portableGameNotation;
	}

	public String getResult() {
		return result;
	}

	public int getSkillLevel() {
		return skillLevel;
	}

	public boolean isPlayerSideWhite() {
		return playerSideWhite;
	}

	public void setBotDepth(int botDepth) {
		this.botDepth = botDepth;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setPlayerSideWhite(boolean playerSideWhite) {
		this.playerSideWhite = playerSideWhite;
	}

	public void setPortableGameNotation(String portableGameNotation) {
		this.portableGameNotation = portableGameNotation;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public void setSkillLevel(int skillLevel) {
		this.skillLevel = skillLevel;
	}

	@Override
	public boolean equals(Object obj) {
		if (null == obj)
			return false;
		if (!(obj instanceof MatchHistoryEntity))
			return false;
		if (this == obj)
			return true;

		MatchHistoryEntity other = (MatchHistoryEntity) obj;
		return (other.botDepth == botDepth) && (other.createdDate.equals(createdDate)) && (other.duration == duration)
				&& (other.id.equals(id)) && (other.playerSideWhite == playerSideWhite)
				&& (other.portableGameNotation.equals(portableGameNotation)) && (other.result.equals(result))
				&& (other.skillLevel == skillLevel);
	}

	@Override
	public int hashCode() {
		return Objects.hash(botDepth, createdDate, duration, id, playerSideWhite, portableGameNotation, result,
				skillLevel);
	}

}
