package com.sangndd.model.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class LocalDateTimeConverter implements Converter<String, LocalDateTime> {

	@Override
	public LocalDateTime convert(String source) {
		String splitTmp[];
		DateTimeFormatter formatter = null;
		LocalDateTime des;

		source = source.trim().replace(' ', 'T').replace('/', '-');
		splitTmp = source.split(":", -1);
		if (splitTmp.length >= 3) { // yyyy-MM-dd HH:mm:ss
			formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
			des = LocalDateTime.parse(source, formatter);
		} else if (splitTmp.length == 2) { // yyyy-MM-dd HH:mm
			formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
			des = LocalDateTime.parse(source, formatter);
		} else if (source.contains("T")) { // yyyy-MM-dd HH
			formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH");
			des = LocalDateTime.parse(source, formatter);
		} else {
			formatter = DateTimeFormatter.ISO_LOCAL_DATE;
			des = LocalDate.parse(source, formatter).atStartOfDay();
		}
		return des;
	}

}
