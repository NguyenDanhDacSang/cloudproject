package com.sangndd.model.utils;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {	
		
	@Override
	public LocalDateTime deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		String splitTmp[];
		DateTimeFormatter formatter = null;
		LocalDateTime des;
		
		String source = p.getText();
		source = source.trim().replace(' ', 'T').replace('/', '-');
		splitTmp = source.split(":", -1);
		if (splitTmp.length >= 3) { // yyyy-MM-dd HH:mm:ss
			formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
			des = LocalDateTime.parse(source, formatter);
		} else if (splitTmp.length == 2) { // yyyy-MM-dd HH:mm
			formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
			des = LocalDateTime.parse(source, formatter);
		} else if (source.contains("T")) { // yyyy-MM-dd HH
			formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH");
			des = LocalDateTime.parse(source, formatter);
		} else {
			formatter = DateTimeFormatter.ISO_LOCAL_DATE;
			des = LocalDate.parse(source, formatter).atStartOfDay();
		}
		return des;
	}

}
