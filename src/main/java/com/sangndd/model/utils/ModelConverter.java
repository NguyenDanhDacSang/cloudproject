package com.sangndd.model.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.LinkedList;
import java.util.List;

import com.sangndd.model.dto.ModelDTO;
import com.sangndd.model.dto.matchhistory.MatchHistoryDTO;
import com.sangndd.model.entity.ModelEntity;
import com.sangndd.model.entity.matchhistory.MatchHistoryEntity;

public class ModelConverter {

	public static ModelDTO toDTO(ModelEntity entity)
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		ModelDTO dto = null;

		if (entity instanceof MatchHistoryEntity) {
			dto = new MatchHistoryDTO();
		}

		Field[] entityFields = entity.getClass().getDeclaredFields(); // Get all fields of entity
		Field dtoField = null; // DTO field

		for (Field entityField : entityFields) { // Iterate through all fields of entity
			if (!Modifier.isFinal(entityField.getModifiers())) { // Ignore final fields

				entityField.setAccessible(true); // Access to private fields
				Object value = entityField.get(entity);

				dtoField = dto.getClass().getDeclaredField(entityField.getName());

				dtoField.setAccessible(true);
				dtoField.set(dto, value);

				// Encapsulates
				entityField.setAccessible(false);
				dtoField.setAccessible(false);
			}
		}

		return dto;
	}

	public static ModelEntity toEntity(ModelDTO dto)
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		ModelEntity entity = null;

		if (dto instanceof MatchHistoryDTO) {
			entity = new MatchHistoryEntity();
		}

		Field[] dtoFields = dto.getClass().getDeclaredFields(); // Get all fields of DTO
		Field entityField = null; // Entity field

		for (Field dtoField : dtoFields) { // Iterate through all fields of DTO
			if (!Modifier.isFinal(dtoField.getModifiers())) { // Ignore final fields
				dtoField.setAccessible(true);
				Object value = dtoField.get(dto);

				entityField = entity.getClass().getDeclaredField(dtoField.getName());

				entityField.setAccessible(true);
				entityField.set(entity, value);

				// Encapsulates
				dtoField.setAccessible(false);
				entityField.setAccessible(false);
			}
		}

		return entity;
	}

	public static List<? extends ModelDTO> toDTOs(List<? extends ModelEntity> entities)
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		List<ModelDTO> dtos = new LinkedList<>();

		for (ModelEntity entity : entities) {
			dtos.add(toDTO(entity));
		}

		return dtos;
	}

	public static List<? extends ModelEntity> toEntities(List<? extends ModelDTO> dtos)
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		List<ModelEntity> entities = new LinkedList<>();

		for (ModelDTO dto : dtos) {
			entities.add(toEntity(dto));
		}

		return entities;
	}

}
