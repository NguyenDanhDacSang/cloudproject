package com.sangndd.model.utils;

public class Constant {
	
	public static class MatchResult {		
		public static final String BLACK_WON = "B";
		public static final String DRAW = "D";
		public static final String WHITE_WON = "W";		
	}
	
	public static class OrderBy {
		public static final String BOT_DEPTH = "botDepth";
		public static final String CREATED_DATE = "createdDate";
		public static final String DURATION = "duration";		
		public static final String RESULT = "result";
		public static final String SKILL_LEVEL = "skillLevel";
	}
	
	public static class SortingDirection {		
		public static final String ASCENDING = "ASC";
		public static final String DESCENDING = "DES"; 
	}
		
}
