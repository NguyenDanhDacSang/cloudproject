package com.sangndd.model.dto.matchhistory;

import java.time.LocalDateTime;
import java.util.Objects;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sangndd.model.dto.ModelDTO;
import com.sangndd.model.utils.LocalDateTimeDeserializer;

public class MatchHistoryDTO extends ModelDTO {

	private static final long serialVersionUID = 3796384925297128763L;

	private int botDepth;
	private LocalDateTime createdDate;
	private int duration;
	private String id;
	private boolean playerSideWhite;
	private String portableGameNotation;
	private String result;
	private int skillLevel;

	public MatchHistoryDTO() {
	}

	public int getBotDepth() {
		return botDepth;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public int getDuration() {
		return duration;
	}

	public String getId() {
		return id;
	}

	public String getPortableGameNotation() {
		return portableGameNotation;
	}

	public String getResult() {
		return result;
	}

	public int getSkillLevel() {
		return skillLevel;
	}

	public boolean isPlayerSideBlack() {
		return playerSideWhite;
	}

	public void setBotDepth(int botDepth) {
		this.botDepth = botDepth;
	}

	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setPlayerSideBlack(boolean playerSideBlack) {
		this.playerSideWhite = playerSideBlack;
	}

	public void setPortableGameNotation(String portableGameNotation) {
		this.portableGameNotation = portableGameNotation;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public void setSkillLevel(int skillLevel) {
		this.skillLevel = skillLevel;
	}

	@Override
	public boolean equals(Object obj) {
		if (null == obj)
			return false;
		if (!(obj instanceof MatchHistoryDTO))
			return false;
		if (this == obj)
			return true;

		MatchHistoryDTO other = (MatchHistoryDTO) obj;
		return (other.botDepth == botDepth) && (other.createdDate.equals(createdDate)) && (other.duration == duration)
				&& (other.id.equals(id)) && (other.playerSideWhite == playerSideWhite)
				&& (other.portableGameNotation.equals(portableGameNotation)) && (other.result.equals(result))
				&& (other.skillLevel == skillLevel);
	}

	@Override
	public int hashCode() {
		return Objects.hash(botDepth, createdDate, duration, id, playerSideWhite, portableGameNotation, result,
				skillLevel);
	}

}
