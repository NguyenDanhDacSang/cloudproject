package com.sangndd.model.dto.matchhistory.statistics;

import java.io.Serializable;

public class MatchHistoryStatisticsDTO implements Serializable {
	
	private static final long serialVersionUID = 2225004539970302939L;
	
	private long drewMatch;
	private long lostMatch;
	private long totalMatch;
	private long wonMatch;
	
	public MatchHistoryStatisticsDTO() {	
	}
	
	public long getDrewMatch() {
		return drewMatch;
	}
	
	public long getLostMatch() {
		return lostMatch;
	}
	
	public long getTotalMatch() {
		return totalMatch;
	}
	
	public long getWonMatch() {
		return wonMatch;
	}	
	
	public void setDrewMatch(long drewMatch) {
		this.drewMatch = drewMatch;
	}
	
	public void setLostMatch(long lostMatch) {
		this.lostMatch = lostMatch;
	}
	
	public void setTotalMatch(long totalMatch) {
		this.totalMatch = totalMatch;
	}	
	
	public void setWonMatch(long wonMatch) {
		this.wonMatch = wonMatch;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof MatchHistoryStatisticsDTO)) return false;
		if (this == obj) return true;
		
		MatchHistoryStatisticsDTO other = (MatchHistoryStatisticsDTO) obj;
		return (other.drewMatch == drewMatch)
				&& (other.lostMatch == lostMatch)
				&& (other.totalMatch == totalMatch)
				&& (other.wonMatch == wonMatch);
	}
	
}
